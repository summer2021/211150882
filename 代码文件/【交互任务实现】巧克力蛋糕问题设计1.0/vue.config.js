/*
 * @Author: your name
 * @Date: 2021-06-16 10:51:08
 * @LastEditTime: 2021-06-20 00:59:09
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \project01\vue.config.js
 */
module.exports = {
    publicPath:'./',
    chainWebpack: config => {
        config.module
            .rule('less')
            .test(/\.less$/)
            .oneOf('vue')
            .use('px2rem-loader')
            .loader('px2rem-loader')
            .before('postcss-loader') // this makes it work.
            .options({ remUnit: 153, remPrecision: 8 })
            .end()
    },
}