/*
 * @Author: your name
 * @Date: 2021-06-16 11:11:29
 * @LastEditTime: 2021-07-03 14:29:33
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \project01\src\plugs\element-ui.js
 */
import Vue from 'vue';
import {
    Button,
    Image,
    Dialog,Alert,
    Avatar,Tag,Card,Tooltip,Input,Message,
    Aside,Container,Header,Main,Footer,Drawer
} from 'element-ui'

Vue.use(Button);
Vue.use(Image);
Vue.use(Dialog);
Vue.use(Avatar);
Vue.use(Aside)
Vue.use(Container)
Vue.use(Header)
Vue.use(Main)
Vue.use(Footer)
Vue.use(Alert)
Vue.use(Tag)
Vue.use(Card)
Vue.use(Tooltip)
Vue.use(Input)
Vue.use(Drawer)


Vue.prototype.$message = Message;