/*
 * @Author: your name
 * @Date: 2021-06-16 10:15:24
 * @LastEditTime: 2021-06-18 21:51:24
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \project01\src\main.js
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'lib-flexible'
import './plugs/element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import './config/index.css'

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')