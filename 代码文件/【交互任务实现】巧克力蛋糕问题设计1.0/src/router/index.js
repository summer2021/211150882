/*
 * @Author: your name
 * @Date: 2021-06-16 10:15:24
 * @LastEditTime: 2021-06-19 14:20:43
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \project01\src\router\index.js
 */
import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [{
    path: '/',
    redirect: '/first-step'
},{
    path: '/first-step',
    name:'first',
    component: () =>
        import ('../views/first-step.vue')
},{
    path: '/second-step',
    name:'second',
    component: () =>
        import ('../views/second-step.vue')
},{
    path:'/tenth-step',
    name:'tenth',
    component: () =>
        import ('../views/tenth-step.vue')
},{
    path:'/twentieth-step',
    name:'twentieth',
    component:()=>import('../views/twentieth.vue')
},{
    path:'/operation-main',
    name:'opertion',
    redirect:'/operation-main/fourth-step',
    component: () =>
        import ('../views/operation-main.vue'),
    children:[{
        path:'fourth-step',
        name:'fourth',
        component:()=>
            import ('../views/fourth-step.vue')
    },{
        path:'twelfth-step',
        name:'twelfth',
        component:()=>
            import ('../views/twelfth-step.vue')
    },{
        path:'fourteenth-step',
        name:'fourteenth',
        component:()=>
            import ('../views/fourteenth-step.vue')
    },{
        path:'sixteenth-step',
        name:'sixteenth',
        component:()=>
            import ('../views/sixteenth-step.vue')
    },{
        path:'/eighteenth-step',
        name:'eighteenth',
        component:()=>import('../views/eighteenth-step.vue')
    },{
        path:'/nineteenth-step',
        name:'nineteenth',
        component:()=>import('../views/nineteenth-step.vue')
    }]
}]

const router = new VueRouter({
    routes
})

export default router