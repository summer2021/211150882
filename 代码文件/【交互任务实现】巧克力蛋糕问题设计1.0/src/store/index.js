/*
 * @Author: your name
 * @Date: 2021-06-16 10:15:24
 * @LastEditTime: 2021-06-20 00:44:03
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \project01\src\store\index.js
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    moveState:false,
    moveID:0,
    infoItemList:[{
        id:0,
        title:'参加生日派对的人数',
        type:1,
        prompter:'young',
        content:'我一共邀请了8个小伙伴参加生日派对，算上我自己一共9个人。我们准备的食物要够所有参加派对的人吃！'
    },{
        id:1,
        title:'生日派对地点',
        type:1,
        prompter:'young',
        content:'明天的生日派对在我家举行，我家的地址是光明小区12号楼201室。'
    },{
        id:2,
        title:'生日蛋糕的制作方法',
        type:2,
    },{
        id:3,
        title:'生日派对的时间',
        type:1,
        prompter:'young',
        content:'明天的生日派对会在上午10:00开始，大概在下午17:00结束。'
    }],
    infoItemDockerList:[null,null,null,null],
    x:0,
    y:0,
    viewUpdate:1,
    ingredientsList:[{
      id:0,
      name:'巧克力',
      state:false,
      value:null,
      unit:'克'
    },{
        id:1,
        name:'白糖',
        state:false,
        value:null,
        unit:'克'
    },{
        id:2,
        name:'热水',
        state:false,
        value:null,
        unit:'毫升'
    },{
        id:3,
        name:'面粉',
        state:false,
        value:null,
        unit:'克'
    },{
        id:4,
        name:'盐',
        state:false,
        value:null,
        unit:'克'
    },{
        id:5,
        name:'番茄酱',
        state:false,
        value:null,
        unit:'毫升'
    },{
        id:6,
        name:'鸡蛋',
        state:false,
        value:null,
        unit:'个'
    },{
        id:7,
        name:'大米',
        state:false,
        value:null,
        unit:'克'
    },{
        id:8,
        name:'香草粉',
        state:false,
        value:null,
        unit:'克'
    },{
        id:9,
        name:'奶油',
        state:false,
        value:null,
        unit:'毫升'
    }]
  },
  mutations: {
    RELOAD(state){
        state.moveState = false
        state.moveID = 0
        state.infoItemList = [{
          id:0,
          title:'参加生日派对的人数',
          type:1,
          prompter:'young',
          content:'我一共邀请了8个小伙伴参加生日派对，算上我自己一共9个人。我们准备的食物要够所有参加派对的人吃！'
      },{
          id:1,
          title:'生日派对地点',
          type:1,
          prompter:'young',
          content:'明天的生日派对在我家举行，我家的地址是光明小区12号楼201室。'
      },{
          id:2,
          title:'生日蛋糕的制作方法',
          type:2,
      },{
          id:3,
          title:'生日派对的时间',
          type:1,
          prompter:'young',
          content:'明天的生日派对会在上午10:00开始，大概在下午17:00结束。'
        }]
        state.infoItemDockerList = [null,null,null,null]
        state.ingredientsList = [{
          id:0,
          name:'巧克力',
          state:false,
          value:null,
          unit:'克'
        },{
            id:1,
            name:'白糖',
            state:false,
            value:null,
            unit:'克'
        },{
            id:2,
            name:'热水',
            state:false,
            value:null,
            unit:'毫升'
        },{
            id:3,
            name:'面粉',
            state:false,
            value:null,
            unit:'克'
        },{
            id:4,
            name:'盐',
            state:false,
            value:null,
            unit:'克'
        },{
            id:5,
            name:'番茄酱',
            state:false,
            value:null,
            unit:'毫升'
        },{
            id:6,
            name:'鸡蛋',
            state:false,
            value:null,
            unit:'个'
        },{
            id:7,
            name:'大米',
            state:false,
            value:null,
            unit:'克'
        },{
            id:8,
            name:'香草粉',
            state:false,
            value:null,
            unit:'克'
        },{
            id:9,
            name:'奶油',
            state:false,
            value:null,
            unit:'毫升'
        }]
    },
    SET_MOVE_STATE(state,value){
      state.moveState = value
    },
    SET_X(state,val){
      state.x = val
    },
    SET_Y(state,val){
      state.y = val
    },
    SET_ID(state,val){
      state.moveID = val
    },
    SET_DOCKER(state,id){
      state.infoItemDockerList[id] = state.infoItemList[state.infoItemList.findIndex(item=>item.id==state.moveID)]
      state.infoItemList.splice(state.infoItemList.findIndex(item=>item.id==state.moveID),1)
    },
    DELETE_DOCKER(state,id){
      state.infoItemList.push(state.infoItemDockerList[id])
      state.infoItemDockerList[id] = null
    },
    CHANGE_DOCKER(state,params){
      let middle = state.infoItemDockerList[params.id]
      state.infoItemDockerList[params.id] = state.infoItemDockerList[params.id+params.type]
      state.infoItemDockerList[params.id+params.type] = middle
      state.viewUpdate +=1
    },
    INGREDIENT_STATE_CHANGE(state,id){
      let index =state.ingredientsList.findIndex(item=>item.id==id)
      state.ingredientsList[index].value = null
      state.ingredientsList[index].state = !state.ingredientsList[index].state
    }
  },
  actions: {
  },
  modules: {
  }
})
