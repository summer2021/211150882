## 代码文件文件夹
---    
#### 1.【官网维护与开发】更新和维护文件：

        src
          |- Home
            |- Header.jsx navBar 
            |- static
              |- header.less 
              |- home.less
              |- responsive.less

#### 2.【交互任务实现】房屋面积问题
        所有文件

#### 3.【交互任务实现】巧克力蛋糕问题设计1.0
        所有文件

#### 4.【交互任务维护开发】
        _common
          |- modules
            |- integrationAPI.01
              |- installationAPI.01
                |- pemFioi
                  |- stepGuidance.css
                  |- stepGuidance.js
                  |- buttonsAndMessages.js
                  |- answerTypes.js
                  |- answerTypes.css
        bebras
          |- 2019
            所有子任务维护更新

#### 5.【命题与题库系统前端】
        所有文件


## 提交的PRs:

### 仓库：https://github.com/open-ct/OpenItem

    # PRs: 
| Repo | Title | Status | 
| ------ | ------ |------ |
| <a href=https://github.com/open-ct/OpenItem/pull/1 target=_blank>OpenItem#1</a> | feat: initialize the front end of proposition and question bank system| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) | 
| <a href=https://github.com/open-ct/OpenItem/pull/2 target=_blank>OpenItem#2</a> | Complete the static page construction of login registration page| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) | 
| <a href=https://github.com/open-ct/OpenItem/pull/3 target=_blank>OpenItem#3</a> | Complete the construction of the static interface of the task listWeb client home| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) | 
| <a href=https://github.com/open-ct/OpenItem/pull/4 target=_blank>OpenItem#4</a> | Complete the proposition guide and test the static interfaceProject management| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) |
| <a href=https://github.com/open-ct/OpenItem/pull/5 target=_blank>OpenItem#5</a> | Complete the static page of the six person interviewInterviews6| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) |
| <a href=https://github.com/open-ct/OpenItem/pull/6 target=_blank>OpenItem#6</a> | Complete the 404 / 402 interface and join the route404 402| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) | 
#### Issues: 
 empty 

#### CodeReview: 
 empty

###仓库：https://github.com/open-ct/openct-landing


| repo | title | status |
| ------ | ------ | ------ |
| <a href=https://github.com/open-ct/openct-landing/pull/5 target=_blank>openct-landing#5</a> | feat: update the official website navbar| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) | 

#### Issues: 
 empty 

##### CodeReview: 
 empty


###仓库：https://gitee.com/openct/bebras-tasks
# PRs: 
| Repo | title | status | 
| ------ | ------ |------ |
| <a href="https://gitee.com/openct/bebras-tasks/pulls/2" target="_blank">bebras-tasks#2</a> | 为bebras-tasks仓库4个子任务添加步骤引导| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) |
| <a href="https://gitee.com/openct/bebras-tasks/pulls/4" target="_blank">bebras-tasks#4</a> | 为bebras-tasks仓库部分任务步骤引导修复bug:响应界面变化| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) |
| <a href="https://gitee.com/openct/bebras-tasks/pulls/7" target="_blank">bebras-tasks#7</a> | 为bebras-tasks仓库2019任务移除成功后提醒| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) | 
| <a href="https://gitee.com/openct/bebras-tasks/pulls/9" target="_blank">bebras-tasks#9</a> | 解决issues：bebras-2019文件内的所有任务的作答成功提示框都上移到操作图形的中心位置| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) |
| <a href="https://gitee.com/openct/bebras-tasks/pulls/11" target="_blank">bebras-tasks#11</a> | 封装步骤引导类适配bebras-2019下所有任务并实现引导状态保持| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) |
| <a href="https://gitee.com/openct/bebras-tasks/pulls/12" target="_blank">bebras-tasks#12</a> | 修复由Git提交产生的异常字符串问题| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) | 
| <a href="https://gitee.com/openct/bebras-tasks/pulls/14" target="_blank">bebras-tasks#14</a> | 修复步骤引导类部分BUG并修改逻辑| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) | 
| <a href="https://gitee.com/openct/bebras-tasks/pulls/15" target="_blank">bebras-tasks#15</a> | 为bebras-2019下base-4-encoding添加步骤引导| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) | 
| <a href="https://gitee.com/openct/bebras-tasks/pulls/16" target="_blank">bebras-tasks#16</a> | bebras-tasks 仓库中移除不必要的调试打印并简化部分逻辑。| ![badge](https://img.shields.io/badge/PR-Open-green?style=for-the-badge&logo=appveyor) |
#### Issues: 
 empty 

#### CodeReview: 
 empty 


    


